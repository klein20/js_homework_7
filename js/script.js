(async function () {
    const responseAlbums = await fetch('https://jsonplaceholder.typicode.com/albums?_limit=10')
    let data = await responseAlbums.json()

    let albums = $('.albums')
    for (const key in data) {
        albums.append(`<div class="album" id="${data[key].id}">
        <p>${data[key].title}</p>
        </div>`)
    }

    let album = $('.album')
    let photos = $('.photos')
    album.on('click', async function () {
        const ALBUM_ID = $(this).attr('id')
        photos.empty()
        const responsePhotos = await fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${ALBUM_ID}&_limit=10`)
        let dataPhotos = await responsePhotos.json()
        for (let photo in dataPhotos) {
            photos.append(`<div class="photo">
            <img src="${dataPhotos[photo].url}" alt="photo">
            <p>${dataPhotos[photo].title}</p>
        </div>`)
        }
    })
})()












